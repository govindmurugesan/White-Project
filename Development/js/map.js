var map; //<-- This is now available to both event listeners and the initialize() function
function loadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false' +
	'&signed_in=true&callback=initialize';
	document.body.appendChild(script);
}
window.onload = loadScript;
function initialize() {
  var mapOptions = {
	   center: new google.maps.LatLng(12.919027900000000000,77.634775699999980000),
	   zoom: 16,
	   mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
}
google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, "resize", function() {
	var center = map.getCenter();
	google.maps.event.trigger(map, "resize");
	map.setCenter(center); 
});