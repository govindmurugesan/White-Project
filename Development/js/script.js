function playVid() { 
	document.getElementById("myVideo").play(); 

	document.getElementById("pause").style.display='block';
	document.getElementById("logo").style.display='none';
} 

function pauseVid() { 
	document.getElementById("myVideo").pause(); 
	document.getElementById("pause").style.display='none';
	document.getElementById("logo").style.display='block';
} 
function showOrHideMenu() {  
	var e = document.getElementById("toggle-menu-item");
   if (e.style.display == 'block' || e.style.display==''){
	   e.style.display = 'none';
   }else {
	   e.style.display = 'block';
   }
}

var map;
function loadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false' +
	'&signed_in=true&callback=initialize';
	document.body.appendChild(script);
}
window.onload = loadScript;
function initialize() {
	var position =  new google.maps.LatLng(12.971180, 77.597253);
  var mapOptions = {
	   center:position,
	   zoom: 16,
	   mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
	var marker = new google.maps.Marker({
        position: position,
        map: map,
        title:"#34, Embassy Diamante, Next to UB City, Vittal Mallya Road, Baangalore- 560001"
    });
	
	var contentString = '<div class="maptext"><h2><b>LOCATE US</b></h2> <h4> #34, <strong>Embassy Diamante</strong>,</h4><h4> Next to UB City, Vittal Mallya Road,</h4><h4> Bangalore- 560001 </h4></div>';
	var infowindow = new google.maps.InfoWindow({
    content: contentString
	});
	infowindow.open(map,marker);
    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });
	
}
google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, "resize", function() {
	var center = map.getCenter();
	google.maps.event.trigger(map, "resize");
	map.setCenter(center); 
});